
\documentclass[11pt]{article}
\usepackage[cmex10]{amsmath}
\usepackage{url}
\usepackage{cite}
\usepackage{fullpage}
\usepackage[pdftex]{graphicx}
\usepackage{graphics}
\usepackage{xspace}
\usepackage{color}
\usepackage{hyperref}
%\usepackage[compact]{titlesec}
\pagestyle{plain}

\newcommand{\eg}{\emph{e.g.}\xspace}
\newcommand{\ie}{\emph{i.e.}\xspace}
\newcommand{\etc}{\emph{etc.}\xspace}
\newcommand{\etal}{{\em et~al.}\xspace}

\begin{document}

%\title{Better bridge distribution strategies}
\title{Toward reputation-based bridge distribution strategies, part one}
\author{Roger Dingledine\\The Tor Project}
\date{}
\maketitle

Reputation-based bridge distribution designs like Salmon~\cite{salmon},
rBridge~\cite{rbridge}, and now Lox~\cite{lox} offer hope of
breaking out of the bridge distribution paradox, which in its simplest
form is: how do you give out bridge addresses to every user who needs one,
without letting the censor learn all of them?

The core idea in using reputation is to keep a history of which bridge
addresses each user has learned, and reward users whose addresses don't
get blocked. But many challenges and tradeoffs have stood in the way of
deploying the idea in practice, and many of them are engineering issues
that don't fit well in research papers so they tend to get skipped over
by the research community.

In 2021 and 2022 we have finally brought together enough building blocks
to make a reputation-based bridge distribution experiment feasible to deploy and
learn from.
In this first document I will describe the components that got us
here:~\S\ref{sec:building-blocks} describes six
building blocks that needed to come into place as a foundation;
then~\S\ref{sec:milestonesA} and~\S\ref{sec:milestonesZ} describe two
upcoming design milestones that we also need as part of the foundation.

%I leave fleshing out the details of the future design to a future document.


\section{Building blocks}
\label{sec:building-blocks}


\paragraph{Building block \#1: a DPI-resistant point-to-point channel.}
The pluggable transports approach~\cite{pluggable-transports}
provides a modular way to plug in methods for transforming Tor traffic in a way
that prevents modern DPI devices from easily recognizing and
blocking traffic flows. The obfs4 protocol~\cite{obfs4}
works well for this purpose in many places around the world, but since
the early days of obfuscated ssh and obfs4 there have been many new
protocols in this same ``look like nothing'' category, such as v2ray's
vmess~\cite{vmess} or any of the similar projects.

Note that at scale, most successful transports rely on the long tail of
buggy or unusual implementations or protocol variations, either to get
close enough to a known protocol or to be far enough from all unwanted
protocols~\footnote{\url{https://lists.torproject.org/pipermail/anti-censorship-team/2019-May/000015.html}}.
Historically, these transports
have been in the awkward situation of ``broken in theory, but works in
practice'' -- though we have future hopes for designs like UPGen to
bring the theory closer to the practice by providing structure to flows
while still providing enough randomness to foil detection rules.

The most important point for this building block though is simply
that these channels do exist, they have been deployed and tested with
real users, and they have been highly successful in practice at avoiding
protocol-based filters.


\paragraph{Building block \#2: a Sybil-resistant sign-up mechanism.} This
issue will forever be a hard open problem, but we at least have some
starts now. This building block aims to identify and exploit resources that (a)
individual users probably have a few of but that it's tough for the censor
to have a whole lot of, and (b) are easy to verify in an automated
way~\cite{tor-blocking}. The original BridgeDB design used https and
gmail distributors (scarce resources: IP addresses on different /16
networks and distinct gmail accounts, respectively),
and later added the domain-fronted ``moat'' channel as well to provide a
more user-oriented option that doesn't require as much sophistication from
users (scarce resource: human attention solve CAPTCHAs). The 2021-2022
Russia censorship episode has also brought us the account-age-based
Telegram distributor (scarce resource: telegram accounts created during
certain timeframes).

The Salmon design paper~\cite{salmon} suggests
requiring the user to pledge their Facebook account. Getting that idea
to work is appealing in principle but nobody has figured out how to make
it work in practice. Progress here would start with
better understanding the available APIs for
popular hub services like Facebook. Which ones let us automate the
process of looking up an account's age or other history, or of scanning
the account for signs of dedicated effort like many posts or many friends
or many pictures -- all ideally without needing to first be friends with
the user?

The key take-away about Sybil-resistant sign-up mechanisms is: the better
we can do at this ``gate keeping'' step, the more flexibility we will
have in the rest of the system.
For example, if we required every user to show up with their own
authenticated Facebook account from 2010 or earlier, we could be much
more lenient about how we treat each user, because it is reasonable to
imagine that the censor won't be able to fabricate that many of them. Or
conversely, if we have only weak mechanisms for preventing floods of
censor-controlled users, then we have to be even more careful at tuning
the bridge allocation process to avoid being overwhelmed by the rate of
incoming users.


\paragraph{Building block \#3: a large pool of diverse bridge addresses.}
A critical piece of any bridge address distribution scheme is the set
of addresses that it distributes. These addresses need to be hard to
discover and/or costly to block. Strategies for making sure an address pool
is resilient include drawing them from networks all around the world
(\eg not just from a single ISP), frequently rotating and refreshing
them (to force the censor to sustain its attacks), and using addresses
that commonly offer other services (to drive up the collateral damage
from leaving a block in place or from overblocking).

We will employ two address pools here. One is the community of thousands
of volunteers who run their own bridges in an organic way. These
bridges are all around the world, and they share networks with many
other services, but because each of them is run by a volunteer, they
tend to be a relatively static population.
The second pool complements this more stable bridge community by using
an automated framework for easily spinning up and down bridge images on
popular cloud providers.

In the future we hope to add a third pool, using the large blocks
of mixed-use addresses (\ie address blocks that are \emph{partially in
use by real users}) offered by Conjure~\cite{conjure}.


\paragraph{Building block \#4: a robust signaling channel.} That is, we
need some bidirectional channel that will reliably work, \ie that censors
won't want to block even when they know the details of how it works. It's
fine if it's low-bandwidth and not-great-latency.

Our primary signaling channel currently is the meek+moat
mechanism in Tor Browser. It uses domain fronting through Fastly, and we have
a few CDNs in mind as backups if Fastly changes its policy about allowing
domain fronting.

While hopefully domain fronting will remain a viable signaling approach
for many years to come, we still need to keep exploring entirely
different mechanisms~\cite{fifield-oss}, in case the whole field of
domain fronting collapses. Currently we have ``proxy through Google AMP
cache'' as a ready backup and we're evaluating ``tunnel through DoH''
as another backup. There are also in-progress research options such as
the Raven email transport~\cite{pets22-raven} which offer different points
in the tradeoff space.


\paragraph{Building block \#5: a way to establish ground-truth about
a suspected-blocked bridge.}
For a good user experience, we want to be ready to replace broken or
blocked bridges with working ones as soon as they stop working. But if
we want to require our own confirmation of failure before we replace
a bridge, we need a mechanism for rapidly (close to real-time) assessing
reachability.

The original 2011 ``Five ways to test
bridge reachability'' blog post~\cite{five-ways} laid out the options:
two passive, two active, and an indirect reflection-based idea which
eventually led to Censored Planet's Spooky-scan design~\cite{spooky-scan}.

We first imagined that we could use passive statistics at each bridge
to discover when they got blocked. But with low per-bridge loads,
each bridge by itself doesn't provide enough signal. Also, aggregate
reporting from bridges will typically provide an answer many hours after
the blocking event.

% The other passive reporting
%option is from the clients themselves, who could report non-working
%bridges via the signaling channel.

Instead, the most straightforward way to establish reliable reachability
ground truth is to get a computer inside each censored area and try
to connect to each bridge. But the act of connecting to a bridge from
inside the censored area is itself a risk, and the more bridges we test
in this manner the greater the risk.

Our plan for mitigating this ``surface area'' exposure is to use
heuristics to triage the list first, with the goal that the direct scans
only risk revealing bridges that are probably already blocked.

Specifically, we have two heuristics in mind that we think will be most
effective at reducing the number of tests we need to do from sensitive
locations: (1) general up/down testing from safer vantage points, and
(2) having clients use the signaling channel to self-report when their
bridges aren't working.

The scaling challenges for bulk up/down testing might mean we want
different testing tools or different configurations of testing tools. In
particular, if we want to detect ``up to down'' state transition within a
few seconds, the naive testing approach would mean we launch thousands
of handshakes every second in order to always notice quickly. A better
design would \emph{hold open} a connection to every bridge, and then
re-test connections right when they fail.

Our main reachability testing tool is Bridgestrap, which runs alongside
a Tor+obfs4proxy client. It receives new bridges to test, changes Tor's
configuration to use those bridges, and then reads out the results as Tor
reports them. As of December 2022, Arti (the new Tor rewrite in Rust)
supports using bridges and pluggable transports, so a promising route
for long-term sustainability is to port the Bridgestrap functionality
over to using Arti's reachability tests as its core. Then we would have
a farm of these reachability testers around the world, and coordinate
them to measure all bridges from several safe locations. When several
clients tell us a bridge is down, yet it appears to be up from our safe
locations, then it would be time to launch a confirmation measurement
from the sensitive location.
%plus measuring only the suspect bridges (several clients think they are
%down, yet they appear up from the safe locations) from the sensitive locations.

%When a request
%comes in from a client to replace a bridge that the client can no longer
%reach, we would follow these steps:
%\begin{itemize}
%\item First, check if the reachability tests from safe locations think it's
%down. If yes,
%we would first check if the bridge 


\paragraph{Building block \#6: a way to know how much load a bridge is
seeing}.
Many of the more advanced bridge distribution designs involve
a feedback loop where we make future bridge assignment decisions based
on past user behavior.

For each bridge we want to learn both \emph{how many users it has}
and \emph{how much bandwidth it has used}.
With this usage data we can identify (a) bridges with high load, which
is related to the efficacy of the bridge at helping users, and (b)
bridges with low user load, for cases where we want to limit the number
of active users on a bridge but give it out to more people when many of
its users have gone inactive.

%Milestone \#3: Dynamically adjust bridge pool sizes (inspired
%by Proximax.



\section{Milestone \#1: Tor Browser integration and automation.}
\label{sec:milestonesA}

With these six building blocks in place, Tor Browser now uses the
signaling channel to learn the recommendations for your country: we serve
a json file with an ordered list of which Pluggable Transports and which
bridge distribution strategies are expected to work best in each country,
and then Tor Browser walks the user through trying them. As of late 2022 we
cover China, Egypt, Iran, Turkey, Belarus, and Turkmenistan.

Browser integration is critical to leading users to the right
configuration for their situation, without requiring too much advance
knowledge of Tor's suite of anti-censorship options.

The remaining work for this milestone is automation: Tor Browser needs
to get better at self-assessing whether Tor has succeeded or failed at
bootstrapping in a given configuration, so it can automatically move to
try the next configuration in the list.

\section{Milestone \#2: Bridge ``subscription'' model.}
\label{sec:milestonesZ}

Some of our volunteer bridges are short-lived: users find them and
happily use them, but then the bridges vanish, either because the
volunteer doesn't keep them running or because they have moved to a
new dynamic IP address. This issue is less about censorship and more a
standard resource reliability issue.

Once we have the Tor Browser integration in place, the next step is
that Tor Browser uses the signaling channel to ask for a replacement
bridge. Requests can use the old bridge info as the ``credential'' to
prove that they already performed the proof-of-resource step, so the
entire process can happen in the background.

;;really two different problems here, and their answers need to be different:
;;one is bridge *failure* and the other is bridge *censorship*.
The subscription model makes the dynamic cloud bridge approach much more
powerful, because if a bridge gets blocked anywhere, you can spin it down
and spin up a new one somewhere else, and users will be able to follow
their bridge. But there is a tradeoff, because it means censors can block
a bridge they know and then follow it and block its replacement, etc.



\section{Trust-based bridge distribution}

% Milestone \#3, still in the research phase:

% (designs like Salmon, rBridge, Hyphae).

Once we have Tor Browser doing
automated background connections to maintain your bridges, the next
step is rather than just replacing bridges when they die, instead track
whether the bridges you gave somebody got blocked. Get rid of people
whose bridges tend to get blocked, and reward people whose bridges last
a long time by giving them new fresh bridges.

Many open problems remain here. For example, it's all about picking the
right parameters so good users get enough bridges yet bad people can't
find too many. Simulations from the Salmon design argue that you really
need a way for trusted users to invite their friends immediately into
higher trust levels, and where if a trusted user misbehaves then you blame
the person who invited them too. But then you need a privacy-preserving
identity management / social graph management mechanism, else the
central database is a juicy target that lets attackers learn who invited
whom. Despite these challenges, I think this is where the arms race
needs to go, because we need to focus on exploiting asymmetries in the
face of well-funded censors.

Understanding the parameter space (how many risks you can take on a new
user, how quickly and thoroughly you have to punish them for having their
address blocked, etc) is a key open challenge. The reality is that we
will need to gain intuition from a real-world deployment, and while we're
learning we should keep in mind that the parameters for the reputation
design need to be tuned together with all of the other components.


Building block 7 a privacy-preserving identity management mechanism on
our side. Or, do we really need to record the recommendation graph in
order for a reputation system to work?

\bibliographystyle{plain} \bibliography{references}
\end{document}





2. system inputs that aren't under our control

2.1 characteristics of honest users

rate of arrival, and rate of use, and rate of losing interest

maybe assume gaussian arrival rate, but with bursts where the rate
goes high?

2.2 characteristics of attacking users

2.3 characteristics of our volunteer bridge pool

in particular, how often we get new ones, how long they last, how
reliable they are while up


3. metrics, i.e. how to know if we're succeeding

3.1 total number of bridges we use over time to handle a given user base

3.2 how often each user needs a rescue

3.3 how many ground-truth tests we need to do in-country


4. straw-man design proposals

to illustrate why we need each of the building blocks, what will be missing

4.1 each user gets their own bridge

4.2 telegram auto-responder

